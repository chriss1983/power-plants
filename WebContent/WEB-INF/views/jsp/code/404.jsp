<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>this page doesn't exist</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="hero-unit center">
					<h1>
						this page doesn't exist <small><font face="Tahoma"
							color="red">Error 404</font></small>
					</h1>
					<br />
					<p>
						${msg_404}
					</p>
				</div>

			</div>
		</div>
	</div>

</body>
</html>