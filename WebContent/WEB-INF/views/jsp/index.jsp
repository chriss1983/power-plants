<%@page import="org.springframework.web.servlet.ModelAndView"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="de">
<head>
<title>Vantago</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"></link>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
	<div class="jumbotron">
		<div class="container">
			<table class="table table-striped table-dark">
				<thead>
					<tr>
						<th scope="col">ID</th>
						<th scope="col">Bild der Anlage</th>
						<th scope="col">Typ der Erzeugungsanlage</th>
						<th scope="col">Leistung (KW)</th>
						<th scope="col">Anschaffungsdatum</th>
						<th scope="col">Hersteller</th>
						<th scope="col">Kaufpreis</th>
						<th scope="col">Einsatzort</th>
						<th scope="col">Betriebsdauer (Std)</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${powerPlants}" var="power">
						<tr>
							<td><c:out value="${power.id}" /></td>
							<th scope="col"><img src="${power.unitPicture}" class="img-thumbnail"></th>
							<td><c:out value="${power.type}" /></td>
							<td><c:out value="${power.powerInKW}" /></td>
							<td><c:out value="${power.dateOfPurchase}" /></td>
							<td><c:out value="${power.manufacturer}" /></td>
							<td><c:out value="${power.purchasePrice}" /></td>
							<td><c:out value="${power.location}" /></td>
							<td><c:out value="${power.operatingTime}" /></td>
							<td><a href="/vantago/remove/${power.id}" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> delete </a></td>
							<td><a href="/vantago/edit/${power.id}" class="btn btn-warning"> <span class="glyphicon glyphicon-edit"></span> edit
							</a></td>

						</tr>
					</c:forEach>
				</tbody>
			</table>
			<p>
				<a class="btn btn-primary" data-target="#InputForm" role="button" data-toggle="modal"> <span class="glyphicon glyphicon-plus-sign"></span> Add
					Power Plant
				</a>
				<!-- 				<input type="submit" value="Save Student Information" id="submit" name="submit" class="btn btn-primary pull-right"> -->
			</p>
		</div>
	</div>

	<div class="container">
		<hr>
		<footer>
			<p>&copy; Christian Richter 2018</p>
		</footer>
	</div>
</body>
<!-- Modal input form begin -->
<div class="modal fade" id="InputForm" tabindex="-1" role="main" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" action="dataServlet" role="form" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Neues Kraftwerk</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label col-sm-3" for="text">Type der Anlage</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="type" name="type" placeholder="Typ der Anlage">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-3" for="number">Leistung (KW)</label>
						<div class="col-sm-8">
							<input type="number" class="form-control" id="powerInKW" name="powerInKW" placeholder="Leistung in KW">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-3" for="text">Anschaffungsdatum</label>
						<div class="col-sm-8">
							<input type="date" class="form-control" id="dateOfPurchase" name="dateOfPurchase" placeholder="Anschaffungsdatum">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-3" for="text">Hersteller</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="manufacturer" name="manufacturer" placeholder="Hersteller des Kraftwerkes">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-3" for="text">Kaufpreis</label>
						<div class="col-sm-8">
							<div class="input-group">
								<span class="input-group-addon">&#8364;</span> <input type="number" name="purchasePrice" value="1000" min="0" step="0.01"
									data-number-to-fixed="2" data-number-stepfactor="100" class="form-control currency" id="purchasePrice" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-3" for="text">Einsatzort</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="location" name="location" placeholder="Einsatzort/Standort">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-3" for="text">Betriebsdauer</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="operatingTime" name="operatingTime" placeholder="Betriebsdauer der Anlage">
						</div>
					</div>
					<div class="modal-footer">
						<label class="btn btn-primary col-sm-5" for="my-file-selector">Bild der Anlage Hochladen <input id="my-file-selector" type="file"
							style="display: none" name="file-upload" onchange="$('#upload-file-info').html(this.files[0].name)">

						</label> <span class='label label-info' id="upload-file-info"></span>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<input type="submit" value="Save" id="submit" name="submit" class="btn btn-primary">

					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- Modal input form end -->

<!-- Modal confirm dialog begin -->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="main" aria-labelledby="confirm-delete" aria-hidden="true">
	<div class="modal-dialog">
		<form>
			<div class="modal-content">
				<div class="modal-header">Datensatz L&#246;schen?</div>
				<div class="modal-body">Sind sie Sicher das Sie den gew&#228;hlten Eintrag l&#246;schen m&#246;chten?</div>
				<div class="modal-footer">
					<button class="btn btn-default" data-dismiss="modal">Cancel</button>
					<a class="btn btn-danger btn-ok">Delete</a>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- Modal confirm dialog end -->
</html>