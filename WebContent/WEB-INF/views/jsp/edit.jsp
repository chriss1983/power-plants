<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"></link>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Insert title here</title>
</head>
<body>
	<form class="form-horizontal" action="editServlet" role="form" method="post">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">Kraftwerk bearbeiten</h4>
		</div>
		<div class="modal-body">
			<div class="form-group">
				<label for="disabledInput" class="control-label col-sm-3" for="text">ID</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="disabledInput" name="id" value="${powerPlant.id}">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="text">Type der Anlage</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="type" name="type" value="${powerPlant.type}">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="number">Leistung (KW)</label>
				<div class="col-sm-8">
					<input type="number" class="form-control" id="powerInKW" name="powerInKW" value="${powerPlant.powerInKW}">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="text">Anschaffungsdatum</label>
				<div class="col-sm-8">
					<input type="date" class="form-control" id="dateOfPurchase" name="dateOfPurchase" value="${powerPlant.dateOfPurchase}">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="text">Hersteller</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="manufacturer" name="manufacturer" value="${powerPlant.manufacturer}">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="text">Kaufpreis</label>
				<div class="col-sm-8">
					<div class="input-group">
						<span class="input-group-addon">&#8364;</span> <input type="number" name="purchasePrice" value="1000" min="0" step="0.01"
							value="${powerPlant.purchasePrice}" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control currency" id="purchasePrice" />
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="text">Einsatzort</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="location" name="location" value="${powerPlant.location }">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="text">Betriebsdauer (Jahren)</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="operatingTime" name="operatingTime" value="${powerPlant.operatingTime}">
				</div>
			</div>
			<div class="modal-footer">
				<label class="btn btn-primary col-sm-2" for="my-file-selector">Bild der Anlage Hochladen <input id="my-file-selector" type="file"
					style="display: none" name="file-upload" onchange="$('#upload-file-info').html(this.files[0].name)">

				</label> <span class='label label-info' id="upload-file-info"></span> <a href="/vantago/back" class="btn btn-default"><span
					class="glyphicon glyphicon-backward"></span> Back</a> <input type="submit" value="Save" id="submit" name="submit"
					class="btn btn-primary">

			</div>
		</div>
	</form>
</body>
</html>