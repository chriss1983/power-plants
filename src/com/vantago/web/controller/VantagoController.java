package com.vantago.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.vantago.web.database.RedeyedCoreManager;
import com.vantago.web.model.PowerPlant;
import com.vantago.web.model.VirtualPowerPlant;
import com.vantago.web.service.DatabaseService;

@Controller
@Scope("session")
public class VantagoController {
	public VantagoController() {
		DatabaseService.getInstance();
	}
	
	@RequestMapping(value = "/", method = { RequestMethod.GET, RequestMethod.POST })
	public String index(Map<String, Object> model) {
		VirtualPowerPlant.getInstance().setVirtualPowerPlant(DatabaseService.getInstance().getAllFromDatabase());
		model.put("powerPlants", DatabaseService.getInstance().getAllFromDatabase());
		return "index";
	}
	
	@RequestMapping(value = "/edit/{selectedObjId:.+}", method = { RequestMethod.GET, RequestMethod.POST })
	public String edit(@PathVariable("selectedObjId") String selectedObjId, Map<String, Object> model) {
		model.put("edit", "true");
		List<String> conditions = new ArrayList<>();

		conditions.add("ID='" + selectedObjId + "'");
		List<PowerPlant> powerPlants = RedeyedCoreManager.getDataBaseUtil().getAllEntities(PowerPlant.class, conditions, null, null, null);
		if (!powerPlants.isEmpty()) {
			VirtualPowerPlant.getInstance().setSelectedPowerPlant(powerPlants.get(0));
			List<PowerPlant> newPowerPlantList = new ArrayList<>();
			for (PowerPlant p : VirtualPowerPlant.getInstance().getVirtualPowerPlant()) {
				if (VirtualPowerPlant.getInstance().getSelctedPowerPlant().getId() != p.getId()) {
					newPowerPlantList.add(p);
				} 
			}

			VirtualPowerPlant.getInstance().getVirtualPowerPlant().clear();
			VirtualPowerPlant.getInstance().setVirtualPowerPlant(newPowerPlantList);
			
			model.put("powerPlant", VirtualPowerPlant.getInstance().getSelctedPowerPlant());
			model.put("powerPlants", DatabaseService.getInstance().getAllFromDatabase());
		}
		return "edit";
	}

	@RequestMapping(value = "/remove/{selectedObjId:.+}", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView remove(@PathVariable("selectedObjId") String selectedObjId, Map<String, Object> model) {

		List<String> conditions = new ArrayList<>();

		conditions.add("ID='" + selectedObjId + "'");

		List<PowerPlant> powerPlants = RedeyedCoreManager.getDataBaseUtil().getAllEntities(PowerPlant.class, conditions, null, null, null);
		if (!powerPlants.isEmpty()) {
			PowerPlant selectedPowerPlant = powerPlants.get(0);

			List<PowerPlant> newPowerPlantList = new ArrayList<>();
			PowerPlant tmp = new PowerPlant();
			for (PowerPlant p : VirtualPowerPlant.getInstance().getVirtualPowerPlant()) {
				if (selectedPowerPlant.getId() == p.getId()) {
					tmp = p;
				} else {
					newPowerPlantList.add(p);
				}
			}

			VirtualPowerPlant.getInstance().getVirtualPowerPlant().clear();
			VirtualPowerPlant.getInstance().setVirtualPowerPlant(newPowerPlantList);
			try {
				DatabaseService.getInstance().delete(selectedPowerPlant);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return new ModelAndView("redirect: http://localhost:8080/vantago/");
	}	
	
	@RequestMapping(value = "/del/{selectedObjId:.+}", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView hello(@PathVariable("selectedObjId") String selectedObjId) {
		ModelAndView model = new ModelAndView();
		model.setViewName("index");
		return new ModelAndView("redirect: http://localhost:8080/vantago/");
	}
	
	@RequestMapping(value = "/back", method = { RequestMethod.GET, RequestMethod.POST })
	public String back(Map<String, Object> model) {
		model.put("powerPlants", DatabaseService.getInstance().getAllFromDatabase());
		//return new ModelAndView("redirect: http://localhost:8080/vantago/");
		return "index";
	}
}