package com.vantago.web.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * @author chris
 *
 * Scan for Controllers, Models and other annotations
 */

@Configuration
@ComponentScan({ "com.vantago" })
public class SpringRootConfig {
}