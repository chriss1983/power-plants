package com.vantago.web.service;

import java.util.List;
import java.util.Set;

import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.vantago.web.database.RedeyedCoreManager;
import com.vantago.web.database.annotations.Model;
import com.vantago.web.model.PowerPlant;
import com.vantago.web.model.VirtualPowerPlant;

@Service
@Scope("session")
public class DatabaseService {

	private static final Logger logger = LoggerFactory.getLogger(DatabaseService.class);

	static private DatabaseService instance = new DatabaseService();

	private final String username = "admin";
	private final String password = "admin";
	private final String databaseURL = "jdbc:mysql://localhost:3306/vantago";
	private final String dialect = "MySQLDialect";
	private final String driver = "com.mysql.jdbc.Driver";

	/**
	 * Package to search for Entitys
	 */
	private final String PACKAGE = "com.vantago";

	/**
	 * Database settings Model
	 */

	/**
	 * Set to save the Entitys for HibernateConfigUtil class
	 */
	private Set<Class<?>> annotated;

	public DatabaseService() {
		// JSON Configuration String with username, password, database URL, Dialect and Driver
		String databaseConfig = "{\"username\":\"" + username + "\",\"password\":\"" + password + "\",\"databaseURL\":\"" + databaseURL + "\",\"dialect\":\"" + dialect + "\",\"driver\":\"" + driver
				+ "\"}";
		
		// Scan in PACKAGE for annotated classes for @Model annotation
		annotated = new Reflections(PACKAGE).getTypesAnnotatedWith(Model.class);

		// initalize the Databasehandler and open the session. 
		// this will also create the Tables, if they dont exist in the database
		RedeyedCoreManager.getDataBaseUtil().initDatabaseConnection(databaseConfig, annotated);

	}

	public void editPowerPlant(PowerPlant powerPlant) {
		RedeyedCoreManager.getDataBaseUtil().update(powerPlant);
	}

	public int saveToDatabase(PowerPlant powerPlant) throws Exception {
		logger.debug("Write object to Database " + powerPlant.toString());
		return RedeyedCoreManager.getDataBaseUtil().insertEntity(powerPlant);
	}

	public List<PowerPlant> getAllFromDatabase() {
		return RedeyedCoreManager.getDataBaseUtil().getAllEntities(PowerPlant.class, null, null, null, null);
	}

	public void delete(PowerPlant powerPlant) {
		try {
			RedeyedCoreManager.getDataBaseUtil().deleteEntity(powerPlant);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void update(PowerPlant powerPlant) {
		RedeyedCoreManager.getDataBaseUtil().update(powerPlant);
	}

	public void deleteSelectedPowerPlant() {
		try {
			RedeyedCoreManager.getDataBaseUtil().deleteEntity(VirtualPowerPlant.getInstance().getSelctedPowerPlant());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static DatabaseService getInstance() {
		return instance;
	}

}