package com.vantago.web.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class VirtualPowerPlant {
	
	private PowerPlant selectedPowerPlant;
	private final static VirtualPowerPlant instance = new VirtualPowerPlant();
	
	private List<PowerPlant> virtualPowerPlant = new ArrayList<PowerPlant>();

	public List<PowerPlant> getVirtualPowerPlant() {
		return virtualPowerPlant;
	}

	public void setVirtualPowerPlant(List<PowerPlant> virtualPowerPlant) {
		this.virtualPowerPlant = virtualPowerPlant;
	}
	
	public static VirtualPowerPlant getInstance() {
		return instance;
	}
	
	public void addPowerPlant(PowerPlant powerPlant) {
		virtualPowerPlant.add(powerPlant);
	}
	
	public PowerPlant getSelctedPowerPlant() {
		return selectedPowerPlant;
	}
	
	public void setSelectedPowerPlant(PowerPlant selectedPowerPlant) {
		this.selectedPowerPlant = selectedPowerPlant;
	}
	
	public void removeSelectedPowerPlant() {
		this.selectedPowerPlant = null;
	}
}
