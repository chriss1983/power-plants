package com.vantago.web.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.springframework.context.annotation.Scope;

import com.vantago.web.database.annotations.Model;

@Model
@Entity
@Scope("session")
@Table(name = "POWERPLANT", uniqueConstraints = { @UniqueConstraint(columnNames = { "ID" }) })
public class PowerPlant {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false, unique = true, length = 11)
	private Integer id;
	@Column(name = "UNITPICTURE", length = 20, nullable = true)
	private String unitPicture;
	@Column(name = "TYPE", length = 20, nullable = true)
	private String type; // replace with Enum
	@Column(name = "POWERINKW", length = 20, nullable = true)
	private double powerInKW;
	@Column(name = "DATEOFPURCHASE", length = 20, nullable = true)
	private String dateOfPurchase;
	@Column(name = "MANUFACTURER", length = 20, nullable = true)
	private String manufacturer;
	@Column(name = "PURCHASEPRICE", length = 20, nullable = true)
	private double purchasePrice;
	@Column(name = "LOCATION", length = 20, nullable = true)
	private String location;
	@Column(name = "OPERATINGTIME", length = 20, nullable = true)
	private double operatingTime;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getPowerInKW() {
		return powerInKW;
	}

	public void setPowerInKW(double powerInKW) {
		this.powerInKW = powerInKW;
	}

	public String getDateOfPurchase() {
		return dateOfPurchase;
	}

	public void setDateOfPurchase(String dateOfPurchase) {
		this.dateOfPurchase = dateOfPurchase;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public double getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public double getOperatingTime() {
		return operatingTime;
	}

	public void setOperatingTime(double operatingTime) {
		this.operatingTime = operatingTime;
	}

	public String getUnitPicture() {
		return unitPicture;
	}

	public void setUnitPicture(String unitPicture) {
		this.unitPicture = unitPicture;
	}

	@Override
	public String toString() {
		return "PowerPlant [id=" + id + ", unitPicture=" + unitPicture + ", type=" + type + ", powerInKW=" + powerInKW + ", dateOfPurchase=" + dateOfPurchase + ", manufacturer=" + manufacturer
				+ ", purchasePrice=" + purchasePrice + ", location=" + location + ", operatingTime=" + operatingTime + "]";
	}

}
