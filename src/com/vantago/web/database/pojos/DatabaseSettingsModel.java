package com.vantago.web.database.pojos;

public class DatabaseSettingsModel {
	private String username;
	private String password;
	private String databaseURL;
	private String dialect;
	private String driver;

	public DatabaseSettingsModel() {
		// TODO Auto-generated constructor stub
	}

	public DatabaseSettingsModel(String username, String password, String databaseURL, String dialect, String driver) {
		super();
		this.username = username;
		this.password = password;
		this.databaseURL = databaseURL;
		this.dialect = dialect;
		this.driver = driver;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDatabaseURL() {
		return databaseURL;
	}

	public void setDatabaseURL(String databaseURL) {
		this.databaseURL = databaseURL;
	}

	public String getDialect() {
		return dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	@Override
	public String toString() {
		return "DataBaseConfig [username=" + username + ", password=" + password + ", databaseURL=" + databaseURL
				+ ", dialect=" + dialect + ", driver=" + driver + "]";
	}

}
