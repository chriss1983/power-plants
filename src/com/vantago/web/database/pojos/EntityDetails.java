package com.vantago.web.database.pojos;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;

/**
 * @author Christian Richter
 * @Since 23. Jun. 18
 * @Version 1.0
 */
public class EntityDetails {
	private Class<?>				entityClass;
	private boolean					isEntityObject			= false;
	private String					tableName;
	private String					idColumn;
	private Map<String, Column>		variableColumnMapping	= new HashMap<String, Column>();
	private Map<String, Object>		variableValidations		= new HashMap<String, Object>();
	private Map<String, Class<?>>	variableType			= new HashMap<String, Class<?>>();
	private Map<String, String>		columnVariableMapping	= new HashMap<String, String>();

	public Class<?> getEntityClass() {
		return entityClass;
	}

	public void setEntityClass(Class<?> entityClass) {
		this.entityClass = entityClass;
	}

	public boolean isEntityObject() {
		return isEntityObject;
	}

	public void setEntityObject(boolean isEntityObject) {
		this.isEntityObject = isEntityObject;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getIdColumn() {
		return idColumn;
	}

	public void setIdColumn(String idColumn) {
		this.idColumn = idColumn;
	}

	public Map<String, Column> getVariableColumnMapping() {
		return variableColumnMapping;
	}

	public Map<String, Object> getvariableValidations() {
		return variableValidations;
	}

	public Set<String> getVariableNames() {
		return variableColumnMapping.keySet();
	}

	public String getColumnName(String variableName) {
		return variableColumnMapping.containsKey(variableName) ? variableColumnMapping.get(variableName).name() : null;
	}

	public Map<String, Class<?>> getVariableType() {
		return variableType;
	}

	public void setVariableColumnMapping(String name, Column annotation) {
		variableColumnMapping.put(name, annotation);
	}

	public void setVariableType(String name, Class<?> type) {
		variableType.put(name, type);
	}

	public String getVariableName(String columnName) {
		return columnVariableMapping.get(columnName);
	}

	public void setColumnVariableMapping(String columnName, String variableName) {
		columnVariableMapping.put(columnName, variableName);
	}

	@Override
	public String toString() {
		return "EntityDetails [entityClass=" + entityClass + ", isEntityObject=" + isEntityObject + ", tableName=" + tableName + ", idColumn=" + idColumn + ", variableColumnMapping=" + variableColumnMapping + ", columnValidations=" + variableValidations + "]";
	}

}
