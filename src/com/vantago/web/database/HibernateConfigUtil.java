package com.vantago.web.database;

import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import com.vantago.web.database.pojos.DatabaseSettingsModel;

public class HibernateConfigUtil {
    final static Logger logger = Logger.getLogger(HibernateConfigUtil.class);
    private static SessionFactory sessionFactory;
    private static ServiceRegistry serviceRegistry;
    private static HibernateConfigUtil instance;

    public SessionFactory buildSessionConfigFactory(DatabaseSettingsModel dataBaseConfig, Set<Class<?>> classes) {
        try {
            Configuration configuration = new Configuration();
            Properties props = new Properties();

            props.put("hibernate.dialect", "org.hibernate.dialect." + dataBaseConfig.getDialect());
            props.put("hibernate.connection.driver_class", dataBaseConfig.getDriver());
            props.put("hibernate.connection.url", dataBaseConfig.getDatabaseURL());
            props.put("hibernate.connection.username", dataBaseConfig.getUsername());
            props.put("hibernate.connection.password", dataBaseConfig.getPassword());
            props.put("hibernate.current_session_context_class", "thread");
            props.put("hibernate.hbm2ddl.auto", "update");

            configuration.setProperties(props);
            logger.info("Hibernate Configuration loaded");

            //Set<Class> allClasses = ReflectionUtils.getAllClasses("com.redeyed.pojos");
            //if (allClasses != null) {
            for (Class<?> clazz : classes) {
                configuration.addAnnotatedClass(clazz);
                logger.debug("Add Class -> " + clazz.getName());
            }
//			} else {
//				logger.debug("ERROR CLASSES");
//			}

            logger.debug("Hibernate Java Config serviceRegistry created");
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
            SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);

            if (sessionFactory == null) {
                throw new NullPointerException("sessionFactory cannot be null");
            }
            return sessionFactory;
        } catch (Throwable ex) {
            logger.error("Initial SessionFactory creation failed." + ex.getMessage());
            throw new ExceptionInInitializerError(ex);
        }
    }

    public SessionFactory getSessionConfigFactory() {
        logger.info("getSessionConfigFactory()");
        if (sessionFactory != null) {
            if (sessionFactory.isClosed()) {
                sessionFactory.openSession();
            }

        }
        return sessionFactory;
    }

    public void close() throws Exception {
        if (serviceRegistry != null) {
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
        }
    }

    public static HibernateConfigUtil getInstance() {
        if (instance == null) {
            instance = new HibernateConfigUtil();
        }
        return instance;
    }
}
