package com.vantago.web.database.utils;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vantago.web.database.pojos.DatabaseSettingsModel;

public class JSONMapper {
	final static Logger logger = Logger.getLogger(JSONMapper.class);
	private static JSONMapper jsonMapper;
	private static Gson GSON = new GsonBuilder().create();

	public JSONMapper() {
	}

	public DatabaseSettingsModel createObject(String databaseJsonConfig) {
		DatabaseSettingsModel databaseConfig;
		databaseConfig = GSON.fromJson(databaseJsonConfig, DatabaseSettingsModel.class);
		logger.debug(databaseConfig);
		return databaseConfig;
	}

	public static DatabaseSettingsModel createDatabaseObject(String databaseJsonConfig) {
		if (jsonMapper == null) {
			jsonMapper = new JSONMapper();
		}
		return jsonMapper.createObject(databaseJsonConfig);
	}

}
