package com.vantago.web.database.manager;

import org.springframework.stereotype.Component;

@Component
public interface DatabaseListener {

	void onSaveOrUpdate(Object entityObject);

	void onUpdate(Object playerData);

	void onInsertEntity(Object entity);

	void onDeleteEntity(Object object);

	void onDeleteEntity(Class<?> clazz, int entityId);

	void onGetAllEntities();

	<T> void onGetEntity(Class<T> clazz, int entityId);

//	public void onUpdateTeam(Team teamData, Integer teamID);
}
