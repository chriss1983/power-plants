package com.vantago.web.database.manager;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class DatabaseManager implements DatabaseListener {
    final static Logger logger = Logger.getLogger(DatabaseManager.class);
    List<DatabaseListener> databaseListenerList;

    public DatabaseManager() {
        logger.info("Create instance of DatabaseManager");
        this.databaseListenerList = new ArrayList<>();
    }

    public void addDatabaseListener(DatabaseListener databaseListener) {
        databaseListenerList.add(databaseListener);
    }

    public void removeDatDatabaseMaabaseListener(DatabaseListener databaseListener) {
        databaseListenerList.remove(databaseListener);
    }

    public void onSaveOrUpdate(Object entityObject) {
        for (DatabaseListener databaseListener : databaseListenerList) {
            databaseListener.onSaveOrUpdate(entityObject);
        }
    }

    public void onUpdate(Object playerData) {
        for (DatabaseListener databaseListener : databaseListenerList) {
            databaseListener.onUpdate(playerData);
        }
    }

    public void onInsertEntity(Object entity) {
        for (DatabaseListener databaseListener : databaseListenerList) {
            databaseListener.onInsertEntity(entity);
        }
    }

    public void onDeleteEntity(Object object) {
        for (DatabaseListener databaseListener : databaseListenerList) {
            databaseListener.onDeleteEntity(object);
        }
    }

    public void onDeleteEntity(Class<?> clazz, int entityId) {
        for (DatabaseListener databaseListener : databaseListenerList) {
            databaseListener.onDeleteEntity(clazz, entityId);
        }
    }

    public void onGetAllEntities() {
        for (DatabaseListener databaseListener : databaseListenerList) {
            databaseListener.onGetAllEntities();
        }
    }

    public <T> void onGetEntity(Class<T> clazz, int entityId) {
        for (DatabaseListener databaseListener : databaseListenerList) {
            databaseListener.onGetEntity(clazz, entityId);
        }
    }
}
