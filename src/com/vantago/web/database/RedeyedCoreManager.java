package com.vantago.web.database;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import com.vantago.web.database.manager.DatabaseManager;
import com.vantago.web.database.pojos.DatabaseSettingsModel;
import com.vantago.web.database.query.base.components.GroupBy;
import com.vantago.web.database.query.base.components.OrderBy;
import com.vantago.web.database.query.base.components.Pagination;
import com.vantago.web.database.utils.JSONMapper;

/**
 * 
 * @author chris
 * 
 *         the main database handler with all needed method which is nessasary
 *         for handling tables
 */

@Component
public class RedeyedCoreManager {
	final static Logger logger = Logger.getLogger(RedeyedCoreManager.class);
	private DatabaseManager databaseManager;
	private static RedeyedCoreManager dataBaseUtil;

	private SessionFactory sessionFactory;

	private Session session;
	private DatabaseSettingsModel baseConfig;
	private Set<Class<?>> classes;

	private RedeyedCoreManager() {
		logger.info("Init DateBaseUtil");
		this.databaseManager = new DatabaseManager();
	}

	/**
	 * 
	 * @param databaseJsonConfig - the configuration as JSON-String
	 * @param classes            - the annotated Classes which want to map in the
	 *                           database
	 */
	public void initDatabaseConnection(String databaseJsonConfig, Set<Class<?>> classes) {
		baseConfig = JSONMapper.createDatabaseObject(databaseJsonConfig);
		logger.debug("Database Object is: " + baseConfig.toString());
		this.classes = classes;

		sessionFactory = HibernateConfigUtil.getInstance().buildSessionConfigFactory(baseConfig, classes);

		if (sessionFactory == null) {
			logger.debug("SessionFactory is NULL");
		}
		session = sessionFactory.openSession();
	}

	/**
	 * 
	 * @param clazz      - the mapped table class
	 * @param conditions - conditions for the selected data, like ID='5'
	 * @param orderBy    - set the order of the data like ASC|DESC
	 * @param pagination - set the pagination number
	 * @param groupBy    - set the group by row
	 * @return the list ob founded objects in the database
	 */
	@SuppressWarnings("unchecked")
	public <T> List<T> getAllEntities(Class<T> clazz, List<String> conditions, OrderBy orderBy, Pagination pagination, GroupBy groupBy) {
		databaseManager.onGetAllEntities();
		try {
			String queryString = "FROM " + clazz.getSimpleName() + getConditions(conditions) + getGroupBy(groupBy) + getOrderBy(orderBy);

			if (!session.isOpen()) {
				session = sessionFactory.openSession();
			}
			Query q = session.createQuery(queryString);
			setPagination(q, pagination, queryString);
			return q.list();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			logger.debug(ex.getMessage());
			return new ArrayList<T>();
		}
	}

	private String getGroupBy(GroupBy groupBy) {
		if (groupBy != null) {
			return " GROUP BY " + groupBy.getVariableName() + " ";
		} else {
			return "";
		}
	}

	private void setPagination(Query query, Pagination pagination, String queryString) {
		if (pagination != null && pagination.getPageNumber() != null && pagination.getPageSize() != null) {
			query.setFirstResult((pagination.getPageNumber() - 1) * pagination.getPageSize());
			query.setMaxResults(pagination.getPageSize());
			pagination.setTotalRows(getRowCount(queryString));
		}
	}

	private String getOrderBy(OrderBy orderBy) {
		if (orderBy != null) {
			return " ORDER BY " + orderBy.getVariableName() + " " + (orderBy.isSortAsc() == true ? "ASC" : "DESC");
		} else {
			return "";
		}
	}

	private String getConditions(List<String> conditions) {
		if (conditions != null) {
			StringBuilder conditionsStr = new StringBuilder(" WHERE ");
			for (String condition : conditions)
				conditionsStr.append(condition).append(" AND ");
			return conditionsStr.substring(0, conditionsStr.length() - 5);
		}
		return "";
	}

	public boolean save(Object object) {
		databaseManager.onSaveOrUpdate(object);
		try {
			session.getTransaction().begin();
			session.save(object);
			session.getTransaction().commit();
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
		return true;
	}

	public void update(Object object) {
		databaseManager.onUpdate(object);
		try {
			if (!session.isOpen()) {
				session = sessionFactory.openSession();
			}
			session.getTransaction().begin();
			session.saveOrUpdate(object);
			session.getTransaction().commit();
//            databaseManager.onUpdate(); // needs to fix
//            return updatedObject;

		} catch (HibernateException ex) {
			logger.error(ex.getMessage());
		} finally {
			session.close();
		}

	}

	@SuppressWarnings("unchecked")
	public <T> T getEntity(Class<T> clazz, int entityId) {
		databaseManager.onGetEntity(clazz, entityId);
		try {
			if (!session.isOpen()) {
				session = sessionFactory.openSession();
			}

			session.getTransaction().begin();
			session.get(clazz, entityId);
			session.getTransaction().commit();

			return (T) session.get(clazz, entityId);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw ex; // throw a exception, instead of give anything back
		}
	}

	@Transactional
	public boolean deleteEntity(Class<?> clazz, int entityId) throws Exception {
		databaseManager.onDeleteEntity(clazz, entityId);
		try {
			Object entity = getEntity(clazz, entityId);
			deleteEntity(entity);
			return true;
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw ex;
		}
	}

	@Transactional
	public boolean deleteEntity(Object object) throws Exception {
		databaseManager.onDeleteEntity(object);
		try {
			session.beginTransaction();
			session.delete(object);
			session.getTransaction().commit();
			return true;
		} catch (Exception ex) {
			logger.debug(ex.getMessage());
			session.getTransaction().rollback();
			return false;
		} finally {
			try {
				HibernateConfigUtil.getInstance().close();
			} catch (Exception ex) {
				logger.error(ex.getMessage());
				throw ex;
			}
		}
	}

	public Integer insertEntity(Object entity) {
		databaseManager.onInsertEntity(entity);
		logger.debug("Insert new Entity to Database: " + entity.toString());
		session.getTransaction().begin();
		Integer id = (Integer) session.save(entity);
		session.getTransaction().commit();
		return id;
	}

	private <T> int getRowCount(String queryString) {
		String query = "SELECT COUNT(*) " + queryString;
		return ((Long) this.sessionFactory.getCurrentSession().createQuery(query).uniqueResult()).intValue();
	}

	public <T> int getRowCount(Class<T> clazz) {
		return getRowCount("FROM " + clazz.getSimpleName());
	}

	public static RedeyedCoreManager getDataBaseUtil() {
		if (dataBaseUtil == null) {
			dataBaseUtil = new RedeyedCoreManager();
		}
		return dataBaseUtil;
	}

	public void shutdown() throws SQLException {
		sessionFactory.close(); // if there are no other open connection
	}
}
