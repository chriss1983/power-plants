package com.vantago.web.database.query.base;

public abstract class Query {

	protected Class<?> fromEntityClass;

	public Class<?> getFromEntity() {
		return fromEntityClass;
	}

	public void setFromEntity(Class<?> fromEntityClass) {
		this.fromEntityClass = fromEntityClass;
	}
}
