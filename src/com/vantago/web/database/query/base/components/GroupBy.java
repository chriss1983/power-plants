package com.vantago.web.database.query.base.components;

public class GroupBy {

	private String	variableName;
	private String	i18Label;

	public String getVariableName() {
		return variableName;
	}

	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}

	public String getI18Label() {
		return i18Label;
	}

	public void setI18Label(String i18Label) {
		this.i18Label = i18Label;
	}
}
