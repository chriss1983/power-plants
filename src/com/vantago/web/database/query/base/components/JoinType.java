package com.vantago.web.database.query.base.components;

public enum JoinType {
	JOIN(" JOIN "), OUTER_JOIN(" OUTER JOIN "), LEFT_JOIN(" LEFT JOIN "), RIGHT_JOIN(" RIGHT JOIN "), FULL_JOIN(" FULL JOIN "), NATURAL_JOIN(" NATURAL JOIN "), CROSS_JOIN(" CROSS JOIN ");

	private String joinTypeValue;

	JoinType(String joinTypeValue) {
		this.joinTypeValue = joinTypeValue;
	}

	@Override
	public String toString() {
		return joinTypeValue;
	}
}