package com.vantago.web.database.query.base.components;

@Deprecated
public class TableJoin {
	private Class<?>	firstEntity;
	private Class<?>	secondEntity;
	private String		firstEntityVariableName;
	private String		secondEntityVariableName;
	private JoinType	joinType;

	public Class<?> getFirstEntity() {
		return firstEntity;
	}

	public void setFirstEntity(Class<?> firstEntity) {
		this.firstEntity = firstEntity;
	}

	public Class<?> getSecondEntity() {
		return secondEntity;
	}

	public void setSecondEntity(Class<?> secondEntity) {
		this.secondEntity = secondEntity;
	}

	public String getFirstEntityVariableName() {
		return firstEntityVariableName;
	}

	public void setFirstEntityVariableName(String firstEntityVariableName) {
		this.firstEntityVariableName = firstEntityVariableName;
	}

	public String getSecondEntityVariableName() {
		return secondEntityVariableName;
	}

	public void setSecondEntityVariableName(String secondEntityVariableName) {
		this.secondEntityVariableName = secondEntityVariableName;
	}

	public JoinType getJoinType() {
		return joinType;
	}

	public void setJoinType(JoinType joinType) {
		this.joinType = joinType;
	}

}
