package com.vantago.web.database.query.util;

import java.util.HashMap;
import java.util.Map;

import com.vantago.web.database.pojos.EntityDetails;

public class QueryUtils {

	public static Map<String, String> getVariableColumnMapping(EntityDetails entityDetails) {
		Map<String, String> variableColumnMapping = new HashMap<String, String>();
		for (String variableName : entityDetails.getVariableColumnMapping().keySet()) {
			variableColumnMapping.put(variableName, entityDetails.getVariableColumnMapping().get(variableName).name());
		}
		return variableColumnMapping;
	}
}