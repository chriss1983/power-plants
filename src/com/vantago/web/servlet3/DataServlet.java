package com.vantago.web.servlet3;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.vantago.web.model.PowerPlant;
import com.vantago.web.service.DatabaseService;

@WebServlet("/dataServlet")
public class DataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public DataServlet() {
		super();
	}
	
	@Autowired
	DatabaseService databaseService;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PowerPlant powerPlant = new PowerPlant();

		powerPlant.setId(27);
		powerPlant.setUnitPicture(request.getParameter("file-upload"));
		powerPlant.setType(request.getParameter("type"));
		powerPlant.setPowerInKW(Double.parseDouble(request.getParameter("powerInKW")));
		powerPlant.setDateOfPurchase(request.getParameter("dateOfPurchase"));
		powerPlant.setManufacturer(request.getParameter("manufacturer"));
		powerPlant.setPurchasePrice(Double.parseDouble(request.getParameter("purchasePrice")));
		powerPlant.setLocation(request.getParameter("location"));
		powerPlant.setOperatingTime(Double.parseDouble(request.getParameter("operatingTime")));
		
		try {
			powerPlant.setId(DatabaseService.getInstance().saveToDatabase(powerPlant));
		} catch (Exception e) {
			e.printStackTrace();
		}

		request.setAttribute("name", "test");
		request.setAttribute("msg", "Hello You are invited");
		request.setAttribute("powerPlants", DatabaseService.getInstance().getAllFromDatabase());
		
		getServletContext().getRequestDispatcher("/WEB-INF/views/jsp/index.jsp").forward(request, response);
	}
}
