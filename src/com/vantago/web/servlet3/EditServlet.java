package com.vantago.web.servlet3;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vantago.web.model.PowerPlant;
import com.vantago.web.service.DatabaseService;

@WebServlet("/edit/editServlet")
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public EditServlet() {
		super();
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PowerPlant powerPlant = new PowerPlant();
		
		powerPlant.setUnitPicture(request.getParameter("file-upload"));
		powerPlant.setType(request.getParameter("id"));
		powerPlant.setType(request.getParameter("type"));
		powerPlant.setPowerInKW(Double.parseDouble(request.getParameter("powerInKW")));
		powerPlant.setDateOfPurchase(request.getParameter("dateOfPurchase"));
		powerPlant.setManufacturer(request.getParameter("manufacturer"));
		powerPlant.setPurchasePrice(Double.parseDouble(request.getParameter("purchasePrice")));
		powerPlant.setLocation(request.getParameter("location"));
		powerPlant.setOperatingTime(Double.parseDouble(request.getParameter("operatingTime")));
		
		try {
			DatabaseService.getInstance().saveToDatabase(powerPlant);
			DatabaseService.getInstance().deleteSelectedPowerPlant();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		request.setAttribute("powerPlants", DatabaseService.getInstance().getAllFromDatabase());
		request.setAttribute("saved", "Data Saved!");
		
		getServletContext().getRequestDispatcher("/WEB-INF/views/jsp/index.jsp").forward(request, response);
		return;
	}
}